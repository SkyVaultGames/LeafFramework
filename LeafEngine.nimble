# Package

version       = "0.1.0"
author        = "SkyVaultGames"
description   = "Graphics library for nim"
license       = "MIT"

# Dependencies

requires "nim >= 0.17.2"
requires "https://github.com/ephja/nim-glfw"
requires "https://github.com/nim-lang/opengl"
requires "https://github.com/define-private-public/stb_image-Nim"
