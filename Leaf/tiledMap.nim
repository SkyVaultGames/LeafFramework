#[
    THE PLAN:
        Use istancing to render the tilemap, this may actually be faster then
        constructing a mesh because it uses less data.
]#

import
    graphics,
    renderer2D,
    os,
    base64,
    opengl,
    gameMath,
    strutils,
    json,
    tables

const VertexShaderCode = """
#version 330
layout (location = 0) in vec2 Vertices;
layout (location = 1) in vec2 Uvs;
layout (location = 2) in vec2 Positions;

uniform mat4 projection;
uniform mat4 view;

out vec2 uvs;

void main(){
    uvs = Uvs;
    gl_Position = projection * view * vec4(Vertices, 0.0, 1.0);
}
"""

const FragmentShaderCode = """
#version 330
out vec4 FragColor;
in vec2 uvs;

uniform sampler2D image;

void main(){
    FragColor = texture(image, uvs);
}
"""

type
    TiledObject* = ref object
        x           *: float32
        y           *: float32
        width       *: float32
        height      *: float32
        typeName    *: string
        visible     *: bool
        rotation    *: float32
        id          *: int
        properties  *: Table[string, string]

    TiledLayer* = ref object
        width* , height* : int32
        name        *: string
        visible     *: bool
        x*, y       *: float32
        opacity     *: float
        data        *: seq[int]
        vao, vbo, positionsBuffer: GLuint # Buffers for rendering
        numVertices: int

    TiledObjectGroup* = ref object
         drawOrder  *: string
         name       *: string
         opacity    *: float32
         visible    *: bool
         x          *: float32
         y          *: float32
         objects    *: seq[TiledObject]

    TiledMap* = ref object
        backgroundColor *: Color
        width*, height  *: int32
        version         *: int
        layers          *: seq[TiledLayer]
        objectgroups    *: seq[TiledObjectGroup]
        tilewidth*, tileheight*: int
        image*: Image
        # TODO: lets move this to a resource
        # manager, so that we dont recalculate
        # all of the quads each time we load a map
        quads*: seq[Region]

proc newTiledLayer(): TiledLayer=
    TiledLayer(
        width: 0, height: 0,
        name: "",
        visible: true,
        x: 0, y: 0,
        opacity: 1.0,
        data: newSeq[int]()
    )

proc newTiledObjectGroup(): TiledObjectGroup=
    TiledObjectGroup(
        drawOrder   : "",
        name        : "",
        opacity     : 1,
        visible     : true,
        x           : 0.0,
        y           : 0.0,
        objects     : newSeq[TiledObject]()
    )

proc unloadMap* (map: TiledMap)=
    for group in map.objectgroups.mitems:
        group.objects.setLen(0)
    for layer in map.layers.mitems:
        layer.data.setLen(0)
    map.quads.setLen(0)
    map.layers.setLen(0)

proc loadTiledJsonMap*(path: string, image: Image): TiledMap =
    if fileExists(path) == false:
        echo "loadTiledJsonMap::Error:: cannot find map: ", path
        return nil

    result = TiledMap(
        backgroundColor: Black(),
        width: 0, height: 0,
        version: 1,
        layers: newSeq[TiledLayer](),
        quads: newSeq[Region](),
        objectgroups: newSeq[TiledObjectGroup](),
        image: image
    )

    var data = parseJson(readFile(path))

    if data.hasKey "backgroundColor":
        result.backgroundColor = toColor(hexColorToFloatColor(data["backgroundcolor"].getStr()))
    else:
        # TODO(Dustin):: Sample the background color from the renderer
        result.backgroundColor = Black()

    result.width        = data["width"].getNum().toU32()
    result.height       = data["height"].getNum().toU32()
    result.version      = data["version"].getNum().toU32()
    result.tilewidth    = data["tilewidth"].getNum().toU32()
    result.tileheight   = data["tileheight"].getNum().toU32()

    if result.width == 0 or result.height == 0:
        echo "loadTiledJsonMap::Error:: cannot load map of size zero."
        return

    # This will never happen, but I guess we should be safe
    if result.tilewidth == 0 or result.tileheight == 0:
        echo "loadTiledJsonMap::Error:: cannot load map of tile size zero."
        return

    echo(image.width / result.tilewidth)

    #TODO: Move, generating region data
    for y in 0..<int(image.height / result.tileheight):
        for x in 0..<int(image.width / result.tilewidth):
            result.quads.add newRegion(
                x * result.tilewidth,
                y * result.tileheight,
                result.tilewidth,
                result.tileheight
            )

    #TODO: add local functions to handle the data, so that we dont repeat code
    # if there is encoding and crap
    let layers = data["layers"]
    for layer in layers:
        var tiledLayer = newTiledLayer()
        tiledLayer.name     = layer["name"].getStr()
        tiledLayer.opacity  = layer["opacity"].getFNum()
        tiledLayer.visible  = layer["visible"].getBVal()
        tiledLayer.x        = layer["x"].getFNum()
        tiledLayer.y        = layer["y"].getFNum()

        if layer.hasKey "compression":
            let ctype = layer["compression"].getStr()

            # TODO: support different compression types
            case ctype:
            else:
                echo "Unsupported compression type: ", ctype
        else:
            # No compression
            if layer["type"].getStr() == "objectgroup":
                # Load the object groups.
                var theObjectGroup = newTiledObjectGroup()
                theObjectGroup.opacity  = layer["opacity"].getFNum()
                theObjectGroup.visible  = layer["visible"].getBVal()
                theObjectGroup.x        = layer["x"].getFNum()
                theObjectGroup.y        = layer["y"].getFNum()
                theObjectGroup.drawOrder= layer["draworder"].getStr()
                result.objectgroups.add theObjectGroup

                let objects = layer["objects"]
                for obj in objects:
                    if obj.hasKey "polygon":
                        echo "nLoadTiledJsonMap::Warning:: we dont yet support polygons. This object will not be loaded."
                        continue

                    var theObject        = TiledObject()
                    theObject.x          = obj["x"].getFNum()
                    theObject.y          = obj["y"].getFNum()
                    theObject.width      = obj["width"].getFNum()
                    theObject.height     = obj["height"].getFNum()
                    theObject.visible    = obj["visible"].getBVal()
                    theObject.typeName   = obj["type"].getStr()
                    theObject.rotation   = obj["rotation"].getFNum()
                    theObject.id         = obj["id"].getNum().toU32()
                    theObject.properties = initTable[string, string]()
                    theObjectGroup.objects.add(theObject)

                    if obj.hasKey "properties":
                      for key, prop in obj["properties"].pairs:
                        theObject.properties[key] = prop.getStr()
            else:
                # get the width and height of the current layer
                tiledLayer.width = layer["width"].getNum().toU32()
                tiledLayer.height = layer["height"].getNum().toU32()

                # handle tiles

                if layer.hasKey "encoding":
                    if layer["encoding"].getStr() == "base64":
                        let tileData: string = decode(layer["data"].getStr())
                        # Extract all of the tile data
                        for c in tileData:
                            tiledLayer.data.add int(c)
                else:
                    let arr = layer["data"]
                    for item in arr:
                        tiledLayer.data.add item.getNum.toU32()

        result.layers.add(tiledLayer)

var program: GLuint = 0
var projLoc: GLint = -1
var viewLoc: GLint = -1

proc draw*(map: TiledMap, xoffset: float32 = 0, yoffset: float32 = 0)=
    # TODO(Dustin): Implement this once the renderer uses a background color
    # nSetClearColor(map.backgroundColor)

    if program == 0:
        program = newProgram(
            loadShader(VERTEX_SHADER, VertexShaderCode),
            loadShader(FRAGMENT_SHADER, FragmentShaderCode),
        )
        bindProgram(program)
        projLoc = getLocation(program, "projection")
        viewLoc = getLocation(program, "view")
        bindProgram(0)


    # @Improve
    bindProgram(program)
    var proj = getProjectionMatrix()
    var view = mul(translation(newV3(-100, -100)),scale(newV3(32, 32, 0)))
    setUniform(program, projLoc, proj)
    setUniform(program, viewLoc, view)

    glActiveTexture(GL_TEXTURE0)
    glBindTexture(GL_TEXTURE_2D, map.image.getID())
    glEnable(GL_BLEND)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)



    glDisable(GL_BLEND)
    glBindTexture(GL_TEXTURE_2D, 0)
    bindProgram(0)
