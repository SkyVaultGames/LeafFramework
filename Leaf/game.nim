import
    glfw,
    glfw/wrapper as glfwx,
    opengl,
    tables,
    times

const NUM_FPS_SAMEPLS = 128

type
    Clock* = ref object
        dt*, fps*, timer*: float
        avFps* : float
        avDt* : float
        ticks* : int64
        last: float
        fps_samples: seq[float]

    Timer* = ref object
        callback: proc(): void
        maxTime: float64
        curTime: float64
        paused, repeat, remove: bool

    # Input handler
    Key* = ref object
        state*, last*: int

    InputManager* = ref object of RootObj
        mouse_x, mouse_y: float64
        mouse_dx, mouse_dy: float
        last_mouse_x, last_mouse_y: float

        the_first: bool
        the_block: bool

        last_mouse_left_state, curr_mouse_left_state: bool
        last_mouse_right_state, curr_mouse_right_state: bool

    Scene* = ref object
        update*     : proc(): void
        draw*       : proc(): void
        load*       : proc(): void
        destroy*    : proc(): void

    Game* = ref object of RootObj
        win: glfwx.GLFWwindow
        clock: Clock
        timers: seq[Timer]
        scenes: seq[Scene]
        input: InputManager

proc updateGLWinEvents (game: Game)
proc createGlfwWinAndInitializeOpengl(width: int, height: int, title: string, preOpenProc: proc()): glfwx.GLFWwindow
proc newClock(): Clock
proc newInputManager* (): InputManager

#[
    Game* = ref object of RootObj
        win: glfwx.GLFWwindow
        clock: Clock
        timers: seq[Timer]
        scenes: seq[Scene]
]#

proc newGame* (width, height: int, name: string, preOpenProc: proc() = proc()=discard): Game=
    Game( win: createGlfwWinAndInitializeOpengl(width, height, name, preOpenProc),
          clock: newClock(),
          timers: newSeq[Timer](),
          scenes: newSeq[Scene](),
          input: newInputManager())

proc getWindowSize* (game: Game): auto =
    var width, height: cint = 0
    glfwx.getWindowSize(game.win, addr width, addr height)
    return (cast[int](width), cast[int](height))

proc setWindowSize* (game: Game, width, height: int)=
    glfwx.setWindowSize(game.win, (cint)width, (cint)height)

proc newScene* (load: proc(): void, update: proc(): void, draw: proc(): void, destroy: proc(): void): Scene=
    Scene(
        load: load,
        update: update,
        draw: draw,
        destroy: destroy
    )

proc gotoScene* (game: Game, scene: Scene)=
    if len(game.scenes) > 0:
        let old = game.scenes.pop()
        if old.destroy != nil:
            old.destroy()
    if scene.load != nil:
        scene.load()
    game.scenes.add(scene)

proc pushScene* (game: Game, scene: Scene)= game.scenes.add scene; scene.load()
proc popScene* (game: Game): Scene {.discardable.} =
    game.scenes[game.scenes.high].destroy()
    return game.scenes.pop()

# I dont like having to call this, I think this would be a lot better
# In a module of its own?
proc drawScenes* (game: Game)=
    if len(game.scenes) > 0:
        game.scenes[game.scenes.high].draw()

# Timer stuff
proc newTimer* (mx: float64, repeat: bool, callback: proc(): void): Timer =
    result = Timer(
        callback: callback,
        maxTime: mx,
        curTime: 0,
        repeat: repeat,
        paused: false,
        remove: false
    )

proc set* (timer: Timer, time: float32)=
    timer.curTime = time

proc start* (timer: Timer, game: Game): Timer {.discardable.}=
    game.timers.add(timer)
    return timer

proc pause* (timer: Timer)=
    timer.paused = true

proc play* (timer: Timer)=
    timer.paused = false

proc delete* (timer: Timer)=
    timer.remove = true

proc percent* (timer: Timer): float64=
    if timer.maxTime == 0: return 0
    return (timer.curTime / timer.maxTime)

proc active* (timer: Timer): bool=
    return timer.curTime < timer.maxTime

proc toggle* (timer: Timer)=
    timer.paused = not timer.paused

proc updateTimers(game: Game)=
    for i in countdown(len(game.timers)-1, 0):
        let timer = game.timers[i]

        if timer.remove: game.timers.delete(i); continue
        if timer.paused: continue

        timer.curTime += game.clock.dt
        if timer.curTime >= timer.maxTime:
            timer.callback()
            if timer.repeat == false:
                game.timers.delete(i)
            else:
                timer.curTime = 0


proc newInputManager* (): InputManager=
    return InputManager(
        mouse_x: 0, mouse_y: 0,
        mouse_dx: 0, mouse_dy: 0,
        last_mouse_x: 0, last_mouse_y: 0,
        the_first: false, the_block: false,
        last_mouse_left_state: false, curr_mouse_left_state: false,
        last_mouse_right_state: false, curr_mouse_right_state: false
    )

proc newKey(): Key=
    return Key(state: 0, last: 0)

var keyMap: Table[cint, Key] = initTable[cint, Key]()

proc getMouseX* (game: Game): float= return game.input.mouse_x
proc getMouseY* (game: Game): float= return game.input.mouse_y
proc getMousePos* (game: Game): (float, float)=
    return (game.getMouseX(), game.getMouseY())

proc getMouseDeltaX* (game: Game): float=return game.input.mouse_dx
proc getMouseDeltaY* (game: Game): float=return game.input.mouse_dy

proc isMouseLeftDown* (game: Game): bool=
    var mwin = getCurrentContext()
    return mwin.getMouseButton(0) == 1

proc isMouseRightDown* (game: Game): bool=
    var mwin = getCurrentContext()
    return mwin.getMouseButton(1) == 1

proc isMouseLeftPressed* (game: Game): bool =
    var win = getCurrentContext()
    game.input.curr_mouse_left_state = win.getMouseButton(0) == 1
    if (game.input.curr_mouse_left_state and not game.input.last_mouse_left_state):
        return true
    return false

proc isMouseLeftReleased* (game: Game): bool =
    var win = getCurrentContext()
    game.input.curr_mouse_left_state = win.getMouseButton(0) == 1
    if (not game.input.curr_mouse_left_state and game.input.last_mouse_left_state):
        return true
    return false

proc isMouseRightPressed* (game: Game): bool =
    var win = getCurrentContext()
    game.input.curr_mouse_right_state = win.getMouseButton(1) == 1
    if (game.input.curr_mouse_right_state and not game.input.last_mouse_right_state):
        return true
    return false

proc isMouseRightReleased* (game: Game): bool =
    var win = getCurrentContext()
    game.input.curr_mouse_right_state = win.getMouseButton(1) == 1
    if (not game.input.curr_mouse_right_state and game.input.last_mouse_right_state):
        return true
    return false

proc isKeyPressed* (game: Game, key: glfw.Key): bool=
    var win = getCurrentContext()
    if (game.input.the_block): return false
    var ckey = cast[cint](key)
    if (not keyMap.contains ckey):
        var mykey = newKey()
        keyMap.add ckey, mykey
    else:
        var k = keyMap[ckey]
        k.state = win.getKey(ckey)
        keyMap[ckey] = k
        if (k.state == 1 and k.last == 0):
            return true
        return false

proc isKeyReleased* (game: Game, key: glfw.Key): bool=
    var win = getCurrentContext()
    if (game.input.the_block): return false
    var ckey = cast[cint](key)
    if (not keyMap.contains ckey):
        var mykey = newKey()
        keyMap.add ckey, mykey
    else:
        var k = keyMap[ckey]
        k.state = win.getKey(ckey)
        keyMap[ckey] = k
        if (k.state == 0 and k.last == 1):
            return true
        return false

proc isKeyDown* (game: Game, key: glfw.Key): bool =
    var win = getCurrentContext()
    var ckey = cast[cint](key)
    if not keyMap.contains ckey:
        var k = newKey()
        k.state = win.getKey(ckey)
        k.last = k.state
        keyMap.add ckey, k
    else:
        keyMap[ckey].state = win.getKey(ckey)
        return keyMap[ckey].state == 1

proc update* (input: InputManager, game: Game)=
    var win = getCurrentContext()
    win.getCursorPos(addr input.mouse_x, addr input.mouse_y)

    for key in keyMap.pairs:
        var k = keyMap[key[0]]
        k.last = k.state
        keyMap[key[0]] = k

    if (input.last_mouse_x < 0 or input.last_mouse_y < 0):
        input.last_mouse_x = game.getMouseX()
        input.last_mouse_y = game.getMouseY()
    else:
        var mx = game.getMouseX()
        var my = game.getMouseY()
        input.mouse_dx = mx - input.last_mouse_x
        input.mouse_dy = my - input.last_mouse_y

    input.last_mouse_left_state = input.curr_mouse_left_state
    input.last_mouse_right_state = input.curr_mouse_right_state

    input.the_first = false


# Timer code
proc newClock(): Clock=
    Clock(
        dt: 0, fps: 0, timer: 0,
        avFps: 0, avDt: 0,
        ticks: -1, last: glfwx.getTime(),
        fps_samples: newSeq[float]()
    )

proc getClock* (game: Game): Clock=game.clock

proc update(clock: Clock)=
    let now = glfwx.getTime()
    clock.dt = now - clock.last
    clock.last = now
    if clock.dt != 0:
        clock.fps = 1 / clock.dt
    clock.timer += clock.dt
    clock.ticks += 1

    if clock.avFps == 0: clock.avFps = clock.fps

    let slen = len(clock.fps_samples)
    if slen < NUM_FPS_SAMEPLS:
        clock.fps_samples.add clock.fps
    else:
        var total = 0.0
        for sample in clock.fps_samples:
            total += sample
        clock.avFps = (total / float(slen))
        clock.fps_samples.setLen(0) # fast way of clearing the sequence

proc createGlfwWinAndInitializeOpengl(width: int, height: int, title: string, preOpenProc: proc()): glfwx.GLFWwindow=
    discard glfwx.init()

    glfwx.windowHint(glfwx.CONTEXT_VERSION_MAJOR, 3)
    glfwx.windowHint(glfwx.CONTEXT_VERSION_MINOR, 3)
    glfwx.windowHint(glfwx.OPENGL_PROFILE, glfwx.OPENGL_CORE_PROFILE)
    preOpenProc()

    result = glfwx.createWindow(cint(width), cint(height), cstring(title), nil, nil)
    glfwx.makeContextCurrent(result)

    loadExtensions()
    glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT)

proc initWindow* (width: int, height: int, title: string)=
    discard #openGLWin(width, height, title)

proc isRunning* (game: Game): bool=
    result = glfwx.windowShouldClose(game.win) == 0
    game.updateGLWinEvents()

proc quit* (game: Game)=
    glfwx.setWindowShouldClose(game.win, 1)

proc setVsyncOn* ()= glfwx.swapInterval(3)
proc setVsyncOff* ()= glfwx.swapInterval(0)

proc updateGLWinEvents (game: Game)=
    glfwx.pollEvents()
    glfwx.swapBuffers(game.win)

    game.clock.update()
    game.input.update(game)
    updateTimers(game)

    if len(game.scenes) > 0: game.scenes[game.scenes.high].update()
