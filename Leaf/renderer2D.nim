import
    graphics,
    opengl,
    math,
    tables,
    game,
    gameMath as m

#TODO:
# We need to depth sort the engine
#
#
#

const VertexShaderCode = """
#version 330 core
layout (location = 0) in vec2 Vertices;
layout (location = 1) in vec2 Uvs;

layout (location = 2) in vec4 Diffuse;
layout (location = 3) in vec4 Region;
layout (location = 4) in mat4 Transform;

uniform mat4 projection;
uniform mat4 view;

out vec4 diffuse;
out vec2 uvs;

void main(){
    diffuse = Diffuse;

    uvs.x = (Region.x + (Uvs.x * Region.z));
    uvs.y = (Region.y + (Uvs.y * Region.w));

    float z = Transform[3].z;
    gl_Position = projection * view * Transform * vec4(Vertices, 0.0, 1.0);
    gl_Position.z = z;
}
"""

const FragmentShaderCode = """
#version 330 core

in vec4 diffuse;
in vec2 uvs;

uniform sampler2D image;

out vec4 FragColor;

void main(){
    FragColor = diffuse * texture(image, uvs);
}
"""

# Types
type
    DrawableList = ref object
        transforms *: seq[float32]
        colors     *: seq[float32]
        uvs        *: seq[float32]

    Drawable* = ref object of RootObj
        reg* : Region
        x*, y*, width*, height* : float32

    DrawGroup* = ref object
        image* : Image
        drawables: seq[Drawable]

# Predefine procedures
proc init2D* ()
proc flush2D* (game: Game)

proc drawImage* (img: Image, reg: Region, x:float, y:float, w:float, h:float, c: Color, rot: float = 0.0, layer = 0.0'f32)
proc drawImage* (img: Image, x:float, y:float, w:float, h:float, c: Color)

var
    program: GLuint = 0
    drawables = newTable[GLuint, DrawableList]()
    viewMatrix = m.identity()
    loaded = false
    spriteVao, spriteVbo: GLuint
    pos_vbo, col_vbo, uvs_vbo, reg_vbo: GLuint
    proj_loc:GLint = -1
    view_loc:GLint = -1
    projection: M4 = m.identity()

proc getProjectionMatrix* (): M4    = projection
proc getViewMatrix* (): M4          = viewMatrix

proc init2D* () =
    var verts = @[
        0.0'f32, 1.0, 1.0, 0.0, 0.0, 0.0,
        0.0'f32, 1.0, 1.0, 1.0, 1.0, 0.0
    ]

    var uvs = @[
        0.0'f32, 0.0, 1.0, 1.0, 0.0, 1.0,
        0.0'f32, 0.0, 1.0, 0.0, 1.0, 1.0
    ]

    spriteVao = newVao(true)
    spriteVbo = newVbo(VERTEX_BUFFER, 2, 0, verts)
    uvs_vbo = newVbo(VERTEX_BUFFER, 2, 1, uvs)

    glGenBuffers(1, addr pos_vbo)
    glGenBuffers(1, addr col_vbo)
    glGenBuffers(1, addr uvs_vbo)
    glGenBuffers(1, addr reg_vbo)
    glBindVertexArray(0)

    # Load the program
    program = newProgram(
        loadShader(VERTEX_SHADER, VertexShaderCode),
        loadShader(FRAGMENT_SHADER, FragmentShaderCode)
    )

    proj_loc = getLocation(program, "projection")
    view_loc = getLocation(program, "view")

    loaded = true

proc clear2D* (clear: Color = White())=
    glClearColor(clear.r, clear.g, clear.b, clear.a)
    glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT)

proc flush2D* (game: Game) =
    if not loaded:
        echo "[ERROR]:: Please call init2D before drawing"
        return

    let (w, h) = game.getWindowSize()
    projection = m.ortho(0'f32, float32(w), float32(h), 0'f32, -10'f32, 10'f32)
    var proj_matrix = projection

    bindProgram(program)
    glBindVertexArray(spriteVao)

    glViewport(GLint(0), GLint(0), GLsizei(w), GLsizei(h))
    setUniform(program, proj_loc, proj_matrix)
    setUniform(program, view_loc, view_matrix)
    glActiveTexture(GL_TEXTURE0)

    # Enable alpha blending
    glEnable(GL_BLEND)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

    for id, drawlist in drawables:
        glBindTexture(GL_TEXTURE_2D, id)

        var transforms = drawlist.transforms
        var colors = drawlist.colors
        var uvs = drawlist.uvs

        let plen = len(transforms)
        let clen = len(colors)
        let ulen = len(uvs)

        if clen == 0 or plen == 0 or ulen == 0: continue

        # Colors
        glBindBuffer(GL_ARRAY_BUFFER, col_vbo)
        glBufferData(
            GL_ARRAY_BUFFER,
            cast[GLsizeiptr](sizeof(float32) * clen),
            nil, GL_STREAM_DRAW)

        glBufferSubData(
            GL_ARRAY_BUFFER,
            0,
            sizeof(float32) * clen,
            addr colors[0])

        glEnableVertexAttribArray(2)
        glVertexAttribPointer(2, 4, cGL_FLOAT, GL_FALSE, 4 * sizeof(float32), cast[pointer](0))
        glVertexAttribDivisor(2, 4)
        glBindBuffer(GL_ARRAY_BUFFER, 0)

        # Region
        glBindBuffer(GL_ARRAY_BUFFER, reg_vbo)
        glBufferData(
            GL_ARRAY_BUFFER,
            cast[GLsizeiptr](sizeof(float32) * ulen),
            nil, GL_STREAM_DRAW
        )
        glBufferSubData(
            GL_ARRAY_BUFFER,
            0,
            sizeof(float32) * ulen,
            addr uvs[0]
        )
        glEnableVertexAttribArray(3)
        glVertexAttribPointer(3, 4, cGL_FLOAT, GL_FALSE, 4 * sizeof(float32), cast[pointer](0))
        glVertexAttribDivisor(3, 4)
        glBindBuffer(GL_ARRAY_BUFFER, 0)

        # Positions
        glBindBuffer(GL_ARRAY_BUFFER, pos_vbo)
        glBufferData(
            GL_ARRAY_BUFFER,
            cast[GLsizeiptr](sizeof(float32) * plen),
            nil, GL_STREAM_DRAW)
        glBufferSubData(
            GL_ARRAY_BUFFER,
            0,
            sizeof(float32) * plen,
            addr transforms[0])

        let v4 = GLsizei(4 * sizeof(float32))
        glEnableVertexAttribArray(4)
        glVertexAttribPointer(4, 4, cGL_FLOAT, GL_FALSE, v4, cast[pointer](0))
        glEnableVertexAttribArray(5)
        glVertexAttribPointer(5, 4, cGL_FLOAT, GL_FALSE, v4, cast[pointer](v4))
        glEnableVertexAttribArray(6)
        glVertexAttribPointer(6, 4, cGL_FLOAT, GL_FALSE, v4, cast[pointer](2 * v4))
        glEnableVertexAttribArray(7)
        glVertexAttribPointer(7, 4, cGL_FLOAT, GL_FALSE, v4, cast[pointer](3 * v4))

        glVertexAttribDivisor(4, 1); glVertexAttribDivisor(5, 1);
        glVertexAttribDivisor(6, 1); glVertexAttribDivisor(7, 1);
        glBindBuffer(GL_ARRAY_BUFFER, 0)

        glDrawArraysInstanced(GL_TRIANGLES, 0, 6, GLsizei(plen / 2))
        glBindTexture(GL_TEXTURE_2D, 0)

    bindProgram(0)
    glBindVertexArray(0)

    glDisable(GL_BLEND)

    # Clear the data
    for list in drawables.mvalues:
        list.colors.setLen(0)
        list.transforms.setLen(0)
        list.uvs.setLen(0)

proc drawImage* (img: Image, x:float, y:float, w:float, h:float, c: Color)=
    drawImage(img, newRegion(0, 0, img.width, img.height),x,y,w,h,c)

proc drawImage* (img: Image, x:float, y:float)=
    drawImage(img, newRegion(0, 0, img.width, img.height), x, y, float(img.width), float(img.height), White())

proc drawImage* (img: Image)=
    drawImage(img, newRegion(0, 0, img.width, img.height), 0, 0, float(img.width), float(img.height), White())

proc drawImage* (img: Image, reg: Region, x:float, y:float)=
    drawImage(img, reg, x, y, float(reg.w), float(reg.h), White())

proc drawImage* (img: Image, reg: Region, x:float, y:float, w:float, h:float, c: Color, rot: float = 0.0, layer = 0.0'f32)=
    var model = newM4(
        w, 0, 0, -w/2,
        0, h, 0, -h/2,
        0, 0, 1, layer,
        0, 0, 0, 1
    )

    model = mul(model, rotZ(rot))
    model = mul(model, translation(x + w / 2, y + h / 2, 0))
    model = model.transpose()

    if not drawables.hasKey(img.getID()):
        let list = DrawableList(
            transforms: newSeq[float32](),
            colors    : newSeq[float32](),
            uvs       : newSeq[float32]()
        )
        drawables.add(
            img.getID(),
            list
        )

    var list = drawables[img.getID()]
    for v in model.m:
        list.transforms.add(v)

    list.colors.add(c.r)
    list.colors.add(c.g)
    list.colors.add(c.b)
    list.colors.add(c.a)

    var
        tw = float32(img.width)
        th = float32(img.height)
        rx = float32(reg.x)
        ry = th - float32(reg.y) - float32(reg.h)
        qx = (rx / tw)
        qy = (ry / th)
        qw = (float32(reg.w) / tw)
        qh = (float32(reg.h) / th)

    list.uvs.add(qx)
    list.uvs.add(qy)
    list.uvs.add(qw)
    list.uvs.add(qh)
