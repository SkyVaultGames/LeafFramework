import
    opengl,
    tables,
    Leaf/game,
    Leaf/gameMath,
    Leaf/graphics,
    Leaf/renderer2D,
    Leaf/tiledMap,
    glfw/wrapper as glfwx

var theGame = newGame(1280, 720, "Hello")

let theMap = loadTiledJsonMap("Dungeon_Room_2.json", loadImage "dungeon_tiles.png")
let image = loadImage "test.png"

init2D()
while (theGame.isRunning()):
    let clock = theGame.getClock()
    clear2D(White())

    drawImage(image, newRegion(200, 200, 100, 100), 100, 100)

    theMap.draw()

    # draw(theMap)
    flush2D(theGame)
